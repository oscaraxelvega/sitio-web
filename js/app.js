/* declarar variables en Js*/

let numero=30;
var nombre ="jose lopez";
const PI = 3.1416;

function arribaMouse(){
    let parrafo = document.getElementById('parrafo');
    parrafo.style.color = 'blue';
    parrafo.style.fontSize = '24px';
    parrafo.style.textAlign= 'justify';
}

function salirMouse() {
    let parrafo = document.getElementById('parrafo');
    parrafo.style.color = 'red';
    parrafo.style.fontSize = '16px';
    parrafo.style.textAlign = 'left';
}

function arribaMouse2(){
    let parrafo = document.getElementById('parrafo2');
    parrafo.style.color = 'blue';
    parrafo.style.fontSize = '24px';
    parrafo.style.textAlign= 'justify';
}

function salirMouse2() {
    let parrafo = document.getElementById('parrafo2');
    parrafo.style.color = 'red';
    parrafo.style.fontSize = '16px';
    parrafo.style.textAlign = 'left';
}

function arribaMouse3(){
    let header = document.getElementById('header');
    header.style.color = 'blue';
}

function salirMouse3() {
    let header = document.getElementById('header');
    header.style.color = 'black';
}

function limpiarParrafo() {
    let parrafo = document.getElementById('parrafo');
    parrafo.innerHTML = "";
}